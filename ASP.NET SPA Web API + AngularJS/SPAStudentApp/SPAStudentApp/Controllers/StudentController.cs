﻿using System.Web.Mvc;

namespace SPAStudentApp.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult AddStudent()
        {
            return PartialView("AddStudent");
        }

        public PartialViewResult EditStudent()
        {
            return PartialView("EditStudent");
        }

        public PartialViewResult DeleteStudent()
        {
            return PartialView("DeleteStudent");
        }

        public PartialViewResult ShowAllStudents()
        {
            return PartialView("ShowAllStudents");
        }
    }
}