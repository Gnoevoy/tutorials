﻿app.service("SPACRUDService", function ($http) {

    //Read all Students  
    this.getStudents = function () {
        return $http.get("/api/StudentAPI");
    };

    //Fundction to Read Student by Student ID  
    this.getStudent = function (id) {
        return $http.get("/api/StudentAPI/" + id);
    };

    //Function to create new Student  
    this.post = function (Student) {
        var request = $http({
            method: "post",
            url: "/api/StudentAPI",
            data: Student
        });
        return request;
    };

    //Edit Student By ID   
    this.put = function (id, Student) {
        var request = $http({
            method: "put",
            url: "/api/StudentAPI/" + id,
            data: Student
        });
        return request;
    };

    //Delete Student By Student ID  
    this.delete = function (id) {
        var request = $http({
            method: "delete",
            url: "/api/StudentAPI/" + id
        });
        return request;
    };
});