﻿app.controller("DeleteStudentController", function ($scope, $location, ShareData, SPACRUDService) {

    getStudent();
    function getStudent() {
        var promiseGetStudent = SPACRUDService.getStudent(ShareData.value);

        promiseGetStudent.then(function (response) {
            $scope.Student = response.data;
        },
              function (response) {
                  $scope.error = 'failure loading Student', response;
              });
    }

    $scope.delete = function () {
        var promiseDeleteStudent = SPACRUDService.delete(ShareData.value);

        promiseDeleteStudent.then(function (response) {
            $location.path("/showstudents");
        },
              function (response) {
                  $scope.error = 'failure loading Student', response;
              });
    };

});