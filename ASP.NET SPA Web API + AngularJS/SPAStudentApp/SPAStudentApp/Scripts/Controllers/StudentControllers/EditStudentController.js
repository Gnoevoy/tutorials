﻿app.controller("EditStudentController", function ($scope, $location, ShareData, SPACRUDService) {
    getStudent();

    function getStudent() {
        var promiseGetStudent = SPACRUDService.getStudent(ShareData.value);

        promiseGetStudent.then(function (response) {
            $scope.Student = response.data;
        },
              function (response) {
                  $scope.error = 'failure loading Student', response;
              });
    }

    $scope.save = function () {
        var Student = {
            StudentID: $scope.Student.studentID,
            Name: $scope.Student.name,
            Email: $scope.Student.email,
            Class: $scope.Student.class,
            EnrollYear: $scope.Student.enrollYear,
            City: $scope.Student.city
        };

        var promisePutStudent = SPACRUDService.put($scope.Student.studentID, Student);
        promisePutStudent.then(function (response) {
            $location.path("/showStudents");
        },
              function (response) {
                  $scope.error = 'failure loading Student', response;
              });
    };

});