﻿app.controller('ShowAllStudentsController', function ($scope, $location, SPACRUDService, ShareData) {  
    loadAllStudentsRecords();  
  
    function loadAllStudentsRecords()  
    {  
        var promiseGetStudent = SPACRUDService.getStudents();  
          
        promiseGetStudent.then(
            function (response) { $scope.Students = response.data },
              function (response) {
                  $scope.error = response;
              });  
    }  
  
    //To Edit Student Information  
    $scope.editStudent = function (StudentID) {  
        ShareData.value = StudentID;  
        $location.path("/editStudent");  
    }  
  
    //To Delete a Student  
    $scope.deleteStudent = function (StudentID) {  
        ShareData.value = StudentID;  
        $location.path("/deleteStudent");  
    }  
});  