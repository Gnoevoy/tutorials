﻿var app = angular.module("ApplicationModule", ["ngRoute"]);

app.factory("ShareData", function () {
    return { value: 0 }
});

//Showing Routing  
app.config(function ($routeProvider) {
    $routeProvider.when('/showStudents',
                        {
                            templateUrl: 'Student/ShowAllStudents',
                            controller: 'ShowAllStudentsController'
                        });
    $routeProvider.when('/addStudent',
                        {
                            templateUrl: 'Student/AddStudent',
                            controller: 'AddStudentController'
                        });
    $routeProvider.when("/editStudent",
                        {
                            templateUrl: 'Student/EditStudent',
                            controller: 'EditStudentController'
                        });
    $routeProvider.when('/deleteStudent',
                        {
                            templateUrl: 'Student/DeleteStudent',
                            controller: 'DeleteStudentController'
                        });
    $routeProvider.otherwise(
                        {
                            redirectTo: '/'
                        });
});