﻿using FiltersSample.Filters;
using System.Web.Mvc;

namespace FiltersSample.Controllers
{
    public class HomeController : Controller
    {
        [IndexException]
        public ActionResult Index()
        {
            int[] mas = new int[2];
            mas[6] = 4;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }        
    }
}