﻿using System;
using System.Web.Mvc;

namespace FiltersSample.Filters
{
    public class IndexException : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled && filterContext.Exception is IndexOutOfRangeException)
            {
                filterContext.Result = new RedirectResult("/Content/ExceptionFound.html");
                filterContext.ExceptionHandled = true;
            }
        }
    }
}