﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FiltersSample.Filters
{
    public class MyAuthorizeAttribute : AuthorizeAttribute
    {
        private String[] allowedUsers = new String[] { };
        private String[] allowedRoles = new String[] { };

        public MyAuthorizeAttribute() { }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (!String.IsNullOrEmpty(base.Users))
            {
                allowedUsers = base.Users.Split(new Char[] { ',' });

                for (int i = 0; i < allowedUsers.Length; i++)
                {
                    allowedUsers[i] = allowedUsers[i].Trim();
                }
            }

            if (!String.IsNullOrEmpty(base.Roles))
            {
                allowedRoles = base.Users.Split(new Char[] { ',' });

                for (int i = 0; i < allowedRoles.Length; i++)
                {
                    allowedRoles[i] = allowedRoles[i].Trim();
                }
            }

            return httpContext.Request.IsAuthenticated && User(httpContext) && Role(httpContext);
        }

        private bool User(HttpContextBase httpContext)
        {
            if (allowedUsers.Length > 0)
            {
                return allowedUsers.Contains(httpContext.User.Identity.Name);
            }

            return true;
        }

        private bool Role(HttpContextBase httpContext)
        {
            if (allowedRoles.Length > 0)
            {
                for (int i = 0; i < allowedRoles.Length; i++)
                {
                    if (httpContext.User.IsInRole(allowedRoles[i]))
                        return true;
                }

                return false;
            }

            return true;
        }        
    }
}