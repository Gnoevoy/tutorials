﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FiltersSample.Startup))]
namespace FiltersSample
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
