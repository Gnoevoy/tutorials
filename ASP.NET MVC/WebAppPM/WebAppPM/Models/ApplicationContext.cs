﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace WebAppPM.Models
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationContext() : base("SampleContext") { }

        public static ApplicationContext Create()
        {
            return new ApplicationContext();
        }
    }

}