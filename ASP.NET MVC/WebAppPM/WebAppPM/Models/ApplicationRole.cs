﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;

namespace WebAppPM.Models
{
    public class ApplicationRole : IdentityRole
    {
        public String Description { get; set; }
    }
}