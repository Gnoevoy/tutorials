﻿using System;

namespace WebAppPM.Models
{
    public class EditRoleModel
    {
        public String Id { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
    }
}