﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebAppPM.Models
{
    public class RegisterModel
    {
        [Required]
        public String Email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public String Password { get; set; }
        [Required]
        [Compare("Password", ErrorMessage = "Passwords are not equal!")]
        [DataType(DataType.Password)]
        public String PasswordConfirm { get; set; }
    }
}