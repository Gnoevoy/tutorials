﻿using System;

namespace WebAppPM.Models
{
    public class CreateRoleModel
    {
        public String Name { get; set; }
        public String Description { get; set; }
    }
}