﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebAppPM.Models
{
    public class LoginModel
    {
        [Required]
        public String Email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public String Password { get; set; }
    }
}