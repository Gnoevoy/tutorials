﻿using CodeFirst;
using System;
using System.Data.Entity;
using System.Web.Mvc;

namespace WebAppPM.Controllers
{
    public class ProjectController : Controller
    {
        // Create data context
        SampleContext db = new SampleContext();
                
        public int MyProperty { get; set; }

        [HttpGet]
        public ActionResult UserProject(int? id)
        {
            var user = db.Users.Find(id);
            var projects = user.Projects;
            
            return View(projects);   
        }

        #region Add project

        [HttpGet]
        public ActionResult AddProject()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddProject(Project project)
        {
            // Assign user with Id = 2 to new project
            var user = db.Users.Find(2);

            user.Projects.Add(project);
            project.Users.Add(user);

            db.SaveChanges();

            return View();
        }

        #endregion

        #region Edit project

        [HttpGet]
        public ActionResult EditProject(int? id)
        {
            if (id == null)
                return HttpNotFound();

            Project project = db.Projects.Find(id);
            if (project != null)
                return View(project);

            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult EditProject(Project project)
        {
            db.Entry(project).State = EntityState.Modified;
            db.SaveChanges();
            return View();
        }

        #endregion

        #region Delete project

        [HttpGet]
        public ActionResult Delete(int id)
        {
            Project project = db.Projects.Find(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            return View(project);
        }

        [HttpPost, ActionName("Delete")]
        public String DeleteConfirmed(int id)
        {
            Project project = db.Projects.Find(id);
            if (project == null)
            {
                return "Not found";
            }

            var user = db.Users.Find(2);

            db.Projects.Remove(project);
            user.Projects.Remove(project);
            
            db.SaveChanges();
            return "Project has been removed!";
        }

        #endregion
    }
}