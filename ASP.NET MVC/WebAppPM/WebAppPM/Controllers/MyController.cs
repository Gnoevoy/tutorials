﻿using System.Web.Mvc;
using System.Web.Routing;

namespace WebAppPM.Controllers
{
    public class MyController : IController
    {
        public void Execute(RequestContext requestContext)
        {
            var ip = requestContext.HttpContext.Request.UserHostAddress;
            var responce = requestContext.HttpContext.Response;
            responce.Write("<h2>Ваша ІР-адреса: " + ip + "</h2>");
        }
    }
}