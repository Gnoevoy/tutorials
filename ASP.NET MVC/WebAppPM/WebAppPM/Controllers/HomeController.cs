﻿using CodeFirst;
using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using WebAppPM.Util;

namespace WebAppPM.Controllers
{
    public class HomeController : Controller
    {
        // Create data context
        SampleContext db = new SampleContext();

        public ActionResult Index()
        {
            return View(db.Users);
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            var user = db.Users.Find(id);

            return View(user);
        }

        public ActionResult UserSearch(string login)
        {
            var allUsers = db.Users.Where(u => u.Login.Contains(login)).ToList();
            if (allUsers.Count <= 0)
            {
                return HttpNotFound();
            }
            return PartialView(allUsers);
        }

        #region Edit user

        [HttpGet]
        public ActionResult EditUser(int? id)
        {
            if (id == null)
                return HttpNotFound();

            User user = db.Users.Find(id);
            if (user != null)
                return View(user);

            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult EditUser(User user)
        {
            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        #endregion

        #region Add user

        [HttpGet]
        public ActionResult AddUser()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddUser([Bind(Include = "UserID,Login,Name,Surname")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(user);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            
            return View(user);
        }

        #endregion

        #region Delete user

        [HttpGet]
        public ActionResult Delete(int id)
        {
            User u = db.Users.Find(id);
            if (u == null)
            {
                return HttpNotFound();
            }
            return View(u);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            User u = db.Users.Find(id);
            if (u == null)
            {
                return HttpNotFound();
            }
            db.Users.Remove(u);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        #endregion

        #region Samples

        public String Square(Int32 a = 10, Int32 h = 3)
        {
            Double s = a * h / 2;
            return "<h2>Площа трикутника с основою " + a +
                   " та висотою " + h + " дорівнює " + s + "</h2>";
        }

        public ActionResult GetHtml()
        {
            return new HtmlResult("<h2>Привіт світ!</h2>");
        }

        public ActionResult GetImage()
        {
            String path = "../Images/visualstudio.png";
            return new ImageResult(path);
        }

        public ViewResult SomeMethod()
        {
            ViewBag.Head = "Привіт світ!";
            return View("SomeView");
        }

        public ActionResult Partial()
        {
            ViewBag.Message = "This is partial view.";
            return PartialView();
        }

        #endregion
    }
}