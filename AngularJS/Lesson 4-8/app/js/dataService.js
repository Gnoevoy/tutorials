﻿myApp.factory('dataService', function ($http, $q) {
    return {
        getData: function () {
            var deffered = $q.defer();

            $http({ method: 'GET', url: 'question.json' }).
            success(function (data, status, headers, config) {
                deffered.resolve(data.question);
            }).
            error(function (data, status, headers, config) {
                deffered.reject(status);
            });
            return deffered.promise;
        }
    };
});