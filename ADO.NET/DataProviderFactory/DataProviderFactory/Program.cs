﻿using ProjectManamgementConnectedLayer;
using System;
using System.Configuration;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;

namespace DataProviderFactory
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create connection string with build object
            var connect = new SqlConnectionStringBuilder();
            connect.InitialCatalog = "ProjectManagementDB";
            connect.DataSource = @"ANDRIK-PC";
            connect.ConnectTimeout = 15;
            connect.IntegratedSecurity = true;

            Console.WriteLine("List before delete:");

            ProjectManagementDAL obj = new ProjectManagementDAL();

            SelectUserData(connect, obj);
            obj.OpenConnection(connect.ConnectionString);

            //obj.InsertUser(8, "User8", "Alfred", "Johns");
            //obj.UpdateUserLogin(8, "User10");            
            //obj.ProcessBlackList(false, 8);
            //obj.InsertProject(6, "Project6", "Desc of Project6", "User8");
            obj.DeleteProject(6);
            //obj.DeleteUser(8);
            obj.CloseConnection();

            Console.WriteLine("\nList after delete:\n");
            SelectUserData(connect, obj);

            Console.ReadLine();
        }

        private static void TestMethod()
        {
            var dp = ConfigurationManager.AppSettings["provider"];
            var cnStr = ConfigurationManager.ConnectionStrings["ProjectManagementSqlProvider"].ConnectionString;

            // Getting provider generator 
            var df = DbProviderFactories.GetFactory(dp);

            // Getting connection object
            using (var cn = df.CreateConnection())
            {
                Console.WriteLine("Connection object --> " + cn.GetType().Name);
                cn.ConnectionString = cnStr;
                cn.Open();

                // Create command object
                var cmd = df.CreateCommand();
                Console.WriteLine("Command object --> " + cmd.GetType().Name);
                cmd.Connection = cn;
                cmd.CommandText = "Select * From Users";

                // Print data with data read object
                using (var dr = cmd.ExecuteReader())
                {
                    Console.WriteLine("Data read object --> " + dr.GetType().Name);
                    Console.WriteLine("\n*** Users in table ***\n");
                    while (dr.Read())
                    {
                        Console.WriteLine("-> User #{0} - {1}\n",
                            dr["UserID"], dr["Login"].ToString());
                    }
                }
            }
        }

        private static void SelectUserData(SqlConnectionStringBuilder connect, ProjectManagementDAL obj)
        {
            obj.OpenConnection(connect.ConnectionString);

            var data = obj.GetAllUsersAsData().Select();

            obj.CloseConnection();

            Console.WriteLine("User login\tName\n");
            for (int i = 0; i < data.Length; i++)
            {
                Console.WriteLine("{0,-16}\t{1}", data[i][1], data[i][2]);
            }
        }

        private static void SelectData(SqlConnectionStringBuilder connect, ProjectManagementDAL obj)
        {
            obj.OpenConnection(connect.ConnectionString);

            var users = obj.GetAllUsersAsData().Select().AsEnumerable();
            var ups = obj.GetAllUserProjectAsData().Select().AsEnumerable();
            var projects = obj.GetAllProjectsAsData().Select().AsEnumerable();

            obj.CloseConnection();

            var data = from user in users
                       join up in ups on (Int32)user["UserID"] equals (Int32)up["UserID"]
                       join project in projects on (Int32)up["ProjectID"] equals (Int32)project["ProjectID"]
                       select new
                       {
                           Login = (String)user["Login"],
                           Name = (String)user["Name"],
                           ProjectName = (String)project["ProjectName"],
                           Description = (String)project["Description"],
                       };

            Console.WriteLine("User login\tName\t    Project\tDescription\n");

            foreach (var item in data)
            {
                Console.WriteLine("{0, -16}{1, -10}{2, -10}{3, -20}", item.Login, item.Name, item.ProjectName, item.Description);
            }
        }
    }
}
