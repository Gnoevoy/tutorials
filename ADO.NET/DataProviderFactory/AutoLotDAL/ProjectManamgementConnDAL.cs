﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ProjectManamgementConnectedLayer
{
    public class ProjectManagementDAL
    {
        private SqlConnection connect = null;

        public void OpenConnection(String connectionString)
        {
            connect = new SqlConnection(connectionString);
            connect.Open();
        }

        public void CloseConnection()
        {
            connect.Close();
        }

        //public void InsertUser(Int32 id, String login, String name, String surname)
        //{
        //    // Оператор SQL
        //    var sql = String.Format("Insert Into Users" +
        //           "(UserID, Login, Name, Surname) Values(@UserId, @Login, @Name, @Surname)");

        //    using (var cmd = new SqlCommand(sql, this.connect))
        //    {
        //        // Додати параметри
        //        cmd.Parameters.AddWithValue("@UserId ", id);
        //        cmd.Parameters.AddWithValue("@Login ", login);
        //        cmd.Parameters.AddWithValue("@Name ", name);
        //        cmd.Parameters.AddWithValue("@Surname", surname);

        //        cmd.ExecuteNonQuery();
        //    }
        //}

        public void InsertUser(Int32 id, String login, String name, String surname)
        {
            // SQL Operator
            var sql = String.Format("Insert Into Users" +
                   "(UserID, Login, Name, Surname) Values('{0}', '{1}', '{2}', '{3}')", id, login, name, surname);

            using (var cmd = new SqlCommand(sql, this.connect))
            {
                var param = new SqlParameter();
                param.ParameterName = "@UserID";
                param.Value = id;
                param.SqlDbType = SqlDbType.Int;
                cmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@Login";
                param.Value = login;
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                cmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@Name";
                param.Value = name;
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                cmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@Surname";
                param.Value = surname;
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                cmd.Parameters.Add(param);

                cmd.ExecuteNonQuery();
            }
        }

        public void DeleteUser(Int32 id)
        {
            var sql = String.Format("Delete from Users where UserID = '{0}'", id);

            using (var cmd = new SqlCommand(sql, this.connect))
            {
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    var error = new Exception("This user assigned to project", ex);
                    throw error;
                }
            }
        }

        public void UpdateUserLogin(Int32 id, String newUserLogin)
        {
            var sql = String.Format("Update Users Set Login = '{0}' Where UserID = '{1}'", newUserLogin, id);

            using (var cmd = new SqlCommand(sql, this.connect))
            {
                cmd.ExecuteNonQuery();
            }
        }

        public void InsertProject(Int32 id, String projectName, String description, String login)
        {
            var sqlProj = String.Format("Insert into Projects" +
                   "(ProjectID, ProjectName, Description) Values('{0}', '{1}', '{2}')", id, projectName, description);

            using (var cmd = new SqlCommand(sqlProj, this.connect))
            {
                var param = new SqlParameter();
                param.ParameterName = "@ProjectID";
                param.Value = id;
                param.SqlDbType = SqlDbType.Int;
                cmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@ProjectName";
                param.Value = projectName;
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                cmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@Description";
                param.Value = description;
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                cmd.Parameters.Add(param);

                cmd.ExecuteNonQuery();
            }

            var data = new DataTable();
            var user = String.Format("Select * from Users where Login = '{0}'", login);

            using (var cmd = new SqlCommand(user, this.connect))
            {
                var dr = cmd.ExecuteReader();
                data.Load(dr);

                dr.Close();
            }

            var userID = (Int32)data.Select().FirstOrDefault()["UserID"];

            var sqlUP = String.Format("Insert Into UserProjects" +
                   "(User_UserID, Project_ProjectID) Values('{0}', '{1}')", userID, id);

            using (var cmd = new SqlCommand(sqlUP, this.connect))
            {
                var param = new SqlParameter();
                param.ParameterName = "@User_UserID";
                param.Value = userID;
                param.SqlDbType = SqlDbType.Int;
                cmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@Project_ProjectID";
                param.Value = id;
                param.SqlDbType = SqlDbType.Int;
                cmd.Parameters.Add(param);

                cmd.ExecuteNonQuery();
            }
        }        

        public void DeleteProject(Int32 id)
        {
            var sqlUP = String.Format("Delete from UserProjects where Project_ProjectID = '{0}'", id);

            using (var cmd = new SqlCommand(sqlUP, this.connect))
            {
                cmd.ExecuteNonQuery();
            }

            var sqlProj = String.Format("Delete from Projects where ProjectID = '{0}'", id);

            using (var cmd = new SqlCommand(sqlProj, this.connect))
            {
                cmd.ExecuteNonQuery();
            }
        }

        public DataTable GetAllUsersAsData()
        {
            var users = new DataTable();
            var sql = "Select * from Users";

            using (var cmd = new SqlCommand(sql, this.connect))
            {
                var dr = cmd.ExecuteReader();
                users.Load(dr);

                dr.Close();
            }

            return users;
        }

        public DataTable GetAllUserProjectAsData()
        {
            var up = new DataTable();
            var sql = "Select * from UserProjects";

            using (var cmd = new SqlCommand(sql, this.connect))
            {
                var dr = cmd.ExecuteReader();
                up.Load(dr);

                dr.Close();
            }

            return up;
        }

        public DataTable GetAllProjectsAsData()
        {
            var projects = new DataTable();
            var sql = "Select * from Projects";

            using (var cmd = new SqlCommand(sql, this.connect))
            {
                var dr = cmd.ExecuteReader();
                projects.Load(dr);

                dr.Close();
            }

            return projects;
        }

        public String LookUpUserName(Int32 userId)
        {
            var userName = String.Empty;

            using (var cmd = new SqlCommand("GetUserName", this.connect))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                // Input parameter
                var param = new SqlParameter();
                param.ParameterName = "@UserID";
                param.SqlDbType = SqlDbType.Int;
                param.Value = userId;

                // Default parameters are input
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                // Output parameter
                param = new SqlParameter();
                param.ParameterName = "@Name";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                param.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param);

                // Execute stored procedure
                cmd.ExecuteNonQuery();
                // Return output parameter
                userName = ((String)cmd.Parameters["@Name"].Value).Trim();
            }

            return userName;
        }

        public void ProcessBlackList(Boolean throwEx, Int32 userId)
        {
            var fName = String.Empty;
            var login = String.Empty;
            var lName = String.Empty;

            var cmdSelect = new SqlCommand(String.Format("Select * from Users where UserID = {0}", userId), connect);

            using (var dr = cmdSelect.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    dr.Read();
                    fName = (String)dr["Name"];
                    lName = (String)dr["Surname"];
                    login = (String)dr["Login"];
                }
                else return;
            }

            // Create command object for each step of operation
            var cmdRemove = new SqlCommand(String.Format("Delete from Users where UserID = {0}", userId), connect);
            var cmdInsert = new SqlCommand(String.Format("Insert into BlackList" + "(UserID, Name, Surname, Login) values" +
                "({0}, '{1}', '{2}', '{3}')", userId, fName, lName, login), connect);

            SqlTransaction tx = null;
            try
            {
                tx = connect.BeginTransaction();
                // Include commands in transaction
                cmdInsert.Transaction = tx;
                cmdRemove.Transaction = tx;
                // Execute commands
                cmdInsert.ExecuteNonQuery();
                cmdRemove.ExecuteNonQuery();
                // Imitation error
                if (throwEx)
                {
                    throw new ApplicationException("Помилка бази даних! Транзакція завершена невдало.");
                }

                tx.Commit();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                // Any error rolls back the transaction
                tx.Rollback();
            }
        }
    }
}
