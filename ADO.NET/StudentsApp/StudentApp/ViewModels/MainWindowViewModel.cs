﻿using CodeFirst;
using System.Collections.ObjectModel;
using System.Windows.Input;
using WPFApp.Helpers;
using WPFApp.Repositories;
using WPFApp.Views;

namespace WPFApp.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        private ObservableCollection<User> _users;
        private User _selectedUser;

        public ObservableCollection<User> Users
        {
            get { return _users; }
            set
            {
                _users = value;
                this.OnPropertyChanged("Users");
            }
        }

        public User SelectedUser
        {
            get { return _selectedUser; }
            set
            {
                _selectedUser = value;
                this.OnPropertyChanged("SelectedUser");
            }
        }

        public ICommand ClickAddUser { get; set; }
        public ICommand ClickUpdateUser { get; set; }
        public ICommand ClickDeleteUser { get; set; }

        public MainWindowViewModel()
        {
            ClickAddUser = new Command(arg => AddUser());
            ClickUpdateUser = new Command(arg => UpdateUser());
            ClickDeleteUser = new Command(arg => DeleteUser());

            ReloadUsers();
        }        

        public void ReloadUsers()
        {
            var users = new UserRepository().getAll();
            Users = new ObservableCollection<User>(users);
        }

        public void AddUser()
        {
            var view = new AddUserView
            {
                DataContext = new AddUserViewModel()
            };
            view.ShowDialog();

            ReloadUsers();
        }

        public void UpdateUser()
        {
            if (SelectedUser == null)
                return;

            var user = new UserRepository().get(SelectedUser.UserID);

            var view = new UpdateUserView
            {
                DataContext = new UpdateUserViewModel(user)
            };
            view.ShowDialog();

            ReloadUsers();
        }

        public void DeleteUser()
        {
            if (SelectedUser != null)
            {
                var user = new UserRepository().get(SelectedUser.UserID);
                new UserRepository().delete(user.UserID);

                ReloadUsers();
            }
        }
    }
}
