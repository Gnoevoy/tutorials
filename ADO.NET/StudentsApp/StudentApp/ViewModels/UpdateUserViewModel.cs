﻿using WPFApp.Helpers;
using WPFApp.Repositories;
using System.Windows.Input;
using CodeFirst;

namespace WPFApp.ViewModels
{
    public class UpdateUserViewModel : ViewModelBase
    {
        private int _userID;
        private string _name;
        private string _surname;
        private string _login;

        private User _dataUser;

        public int UserID
        {
            get { return _userID; }
            set
            {
                _userID = value;
                OnPropertyChanged("UserID");
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        public string Surname
        {
            get { return _surname; }
            set
            {
                _surname = value;
                OnPropertyChanged("Surname");
            }
        }

        public string Login
        {
            get { return _login; }
            set
            {
                _login = value;
                OnPropertyChanged("Login");
            }
        }

        public ICommand ClickUpdateUser { get; set; }

        public UpdateUserViewModel(User user)
        {
            _dataUser = user;

            UserID = user.UserID;
            Name = user.Name;
            Surname = user.Surname;
            Login = user.Login;

            ClickUpdateUser = new Command(arg => UpdateUser());
        }

        public void UpdateUser()
        {
            if (_dataUser != null)
            {
                _dataUser.UserID = UserID;
                _dataUser.Name = Name;
                _dataUser.Surname = Surname;
                _dataUser.Login = Login;

                new UserRepository().update(_dataUser);
            }

            Close();
        }

        private void Close()
        {
            CloseView = true;
        }
    }
}
