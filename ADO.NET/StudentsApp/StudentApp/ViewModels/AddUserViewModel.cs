﻿using WPFApp.Repositories;
using System.Windows.Input;
using WPFApp.Helpers;
using CodeFirst;

namespace WPFApp.ViewModels
{
    public class AddUserViewModel : ViewModelBase
    {
        private int _userID;
        private string _name;
        private string _surname;
        private string _login;

        public int UserID
        {
            get { return _userID; }
            set
            {
                _userID = value;
                OnPropertyChanged("UserID");
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        public string Surname
        {
            get { return _surname; }
            set
            {
                _surname = value;
                OnPropertyChanged("Surname");
            }
        }

        public string Login
        {
            get { return _login; }
            set
            {
                _login = value;
                OnPropertyChanged("Login");
            }
        }

        public ICommand ClickAddUser { get; set; }

        public AddUserViewModel()
        {
            ClickAddUser = new Command(arg => AddUser());
        }

        public void AddUser()
        {
            var newUser = new User
            {
                UserID = this.UserID,
                Name = this.Name,
                Surname = this.Surname,
                Login = this.Login
            };

            new UserRepository().add(newUser);

            Close();
        }

        private void Close()
        {
            CloseView = true;
        }
    }
}
