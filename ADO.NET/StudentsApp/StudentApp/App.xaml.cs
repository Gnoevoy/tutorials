﻿using WPFApp.ViewModels;
using System.Windows;

namespace WPFApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            var mw = new MainWindow
            {
                DataContext = new MainWindowViewModel()
            };

            mw.Show();
        }        
    }
}
