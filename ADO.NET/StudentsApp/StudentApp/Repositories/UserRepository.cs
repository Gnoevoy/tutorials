﻿using CodeFirst;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

namespace WPFApp.Repositories
{
    public class UserRepository
    {
        private SampleContext _context;

        public UserRepository()
        {
            _context = new SampleContext();
        }

        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public IEnumerable<User> getAll()
        {
            return _context.Users;
        }

        /// <summary>
        /// Get user by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public User get(int id)
        {
            return _context.Users.SingleOrDefault(s => s.UserID == id);
        }
        /// <summary>
        /// Add new user
        /// </summary>
        /// <param name="spec"></param>
        public void add(User user)
        {
            _context.Users.Add(user);
            _context.SaveChanges();
        }
        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="spec"></param>
        public void update(User user)
        {
            _context.Users.AddOrUpdate(user);
            _context.SaveChanges();
        }
        /// <summary>
        /// Delete user
        /// </summary>
        /// <param name="id"></param>
        public void delete(int id)
        {
            _context.Users.Remove(_context.Users.SingleOrDefault(s => s.UserID == id));
            _context.SaveChanges();
        }
    }
}
