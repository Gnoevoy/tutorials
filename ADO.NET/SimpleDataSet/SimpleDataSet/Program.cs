﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace SimpleDataSet
{
    class Program
    {
        static void Main(string[] args)
        {
            var connect = new SqlConnectionStringBuilder();
            connect.InitialCatalog = "ProjectManagementDB";
            connect.DataSource = @"ANDRIK-PC";
            connect.ConnectTimeout = 15;
            connect.IntegratedSecurity = true;

            var adp = FillDS(connect.ConnectionString);
            var data = GetDS(adp);

            //InsertDS(data, adp, connect.ConnectionString);
            //UpdateDS(8, "User10", data, adp);
            //DeleteDS(5, connect.ConnectionString);            

            PrintDataSet(data);

            Console.ReadKey();
        }

        static void PrintDataSet(DataSet ds)
        {
            // Print each table
            foreach (DataTable item in ds.Tables)
            {
                Console.WriteLine("=> Table: {0}", item.TableName);

                // Print names of columns
                for (int i = 0; i < item.Columns.Count; i++)
                {
                    Console.Write("{0,-15}", item.Columns[i].ColumnName);
                }

                Console.WriteLine("\n-------------------------------------------------------------");

                // Print table content
                PrintTable(item);
            }
        }

        static DataSet GetDS(SqlDataAdapter adp)
        {
            var projDS = new DataSet();
            adp.Fill(projDS, "Projects");
            return projDS;
        }

        static void FillDataSet(DataSet ds)
        {
            var userIDColumn = new DataColumn("UserID", typeof(Int32));
            userIDColumn.Caption = "User ID";
            userIDColumn.ReadOnly = true;
            userIDColumn.AllowDBNull = false;
            userIDColumn.Unique = true;
            userIDColumn.AutoIncrement = true;
            userIDColumn.AutoIncrementSeed = 0;
            userIDColumn.AutoIncrementStep = 1;


            var loginColumn = new DataColumn("Login", typeof(String));
            var nameColumn = new DataColumn("Name", typeof(String));
            var surnameColumn = new DataColumn("Surname", typeof(String));

            // Add DataColumn objects to DataTable
            var userTable = new DataTable("Users");
            userTable.Columns.AddRange(new DataColumn[]
            { userIDColumn, loginColumn, nameColumn, surnameColumn});

            // Add rows to Users
            var userRow = userTable.NewRow();

            userRow["Login"] = "User8";
            userRow["Name"] = "Darrin";
            userRow["Surname"] = "Wong";

            userTable.Rows.Add(userRow);

            userRow = userTable.NewRow();
            userRow["Login"] = "User9";
            userRow["Name"] = "Derek";
            userRow["Surname"] = "Romero";

            userTable.Rows.Add(userRow);

            // Primary key for table 
            userTable.PrimaryKey = new DataColumn[] { userTable.Columns[0] };

            // Add table to DataSet
            ds.Tables.Add(userTable);

        }

        static SqlDataAdapter FillDS(String conn)
        {
            SqlConnection connection = new SqlConnection(conn);

            string query = "Select * from Projects";

            SqlCommand command = new SqlCommand(query, connection);

            SqlDataAdapter adp = new SqlDataAdapter(command);

            return adp;
        }

        static void InsertDS(DataSet ds, SqlDataAdapter adp, String connStr)
        {
            var projRow = ds.Tables["Projects"].NewRow();
            projRow["ProjectID"] = 5;
            projRow["ProjectName"] = "Project5";
            projRow["Description"] = "Desc of Project5";
            ds.Tables["Projects"].Rows.Add(projRow);

            var commandBuilder = new SqlCommandBuilder(adp);

            adp.InsertCommand = commandBuilder.GetInsertCommand();

            adp.Update(ds, "Projects");

            var connection = new SqlConnection(connStr);
            var query = "Select * from UserProjects";
            var command = new SqlCommand(query, connection);

            adp = new SqlDataAdapter(command);
            ds = new DataSet();
            adp.Fill(ds, "User_Project");

            var upRow = ds.Tables["UserProjects"].NewRow();
            upRow["User_UserID"] = 8;
            upRow["Project_ProjectID"] = 5;
            ds.Tables["UserProjects"].Rows.Add(upRow);

            commandBuilder = new SqlCommandBuilder(adp);

            adp.InsertCommand = commandBuilder.GetInsertCommand();

            adp.Update(ds, "UserProjects");
        }

        static void UpdateDS(Int32 id, String login, DataSet ds, SqlDataAdapter adp)
        {
            foreach (DataRow dr in ds.Tables["Users"].Rows)
            {
                if ((Int32)dr["UserID"] == id)
                {
                    dr["Login"] = login;
                }
            }

            var commandBuilder = new SqlCommandBuilder(adp);

            adp.UpdateCommand = commandBuilder.GetUpdateCommand();

            adp.Update(ds, "Users");
        }

        static void DeleteDS(Int32 id, String connStr)
        {
            var connection = new SqlConnection(connStr);
            var query = "Select * from UserProjects";
            var command = new SqlCommand(query, connection);

            var adp = new SqlDataAdapter(command);
            var ds = new DataSet();
            adp.Fill(ds, "UserProjects");

            var row = ds.Tables["UserProjects"].AsEnumerable().Where(o => o.Field<Int32>("Project_ProjectID") == id).FirstOrDefault();
            row.Delete();

            var commandBuilder = new SqlCommandBuilder(adp);

            adp.UpdateCommand = commandBuilder.GetDeleteCommand();

            adp.Update(ds, "UserProjects");

            connection = new SqlConnection(connStr);
            query = "Select * from Projects";
            command = new SqlCommand(query, connection);

            adp = new SqlDataAdapter(command);
            ds = new DataSet();
            adp.Fill(ds, "Projects");

            row = ds.Tables["Projects"].AsEnumerable().Where(o => o.Field<Int32>("ProjectID") == id).FirstOrDefault();
            row.Delete();

            commandBuilder = new SqlCommandBuilder(adp);

            adp.UpdateCommand = commandBuilder.GetDeleteCommand();

            adp.Update(ds, "Projects");
        }

        static void DeleteDS(Int32 id, DataSet ds, SqlDataAdapter adp)
        {
            foreach (DataRow dr in ds.Tables["Users"].Rows)
            {
                if ((Int32)dr["UserID"] == id)
                {
                    dr.Delete();
                }
            }

            var commandBuilder = new SqlCommandBuilder(adp);

            adp.DeleteCommand = commandBuilder.GetDeleteCommand();

            adp.Update(ds, "Users");
        }

        static void PrintTable(DataTable dt)
        {
            // Create DataTableReader object
            var dtReader = dt.CreateDataReader();

            while (dtReader.Read())
            {
                for (int i = 0; i < dtReader.FieldCount; i++)
                {
                    Console.Write("{0,-15}", dtReader.GetValue(i).ToString().Trim());
                }
                Console.WriteLine();
            }

            dtReader.Close();
        }
    }
}
