﻿using System.Data.Entity;

namespace CodeFirst
{
    public class SampleContext : DbContext
    {
        // Name of future database can be specified by
        // call base class constructor
        public SampleContext() : base()
        { }

        // Displaying database tables on the properties of the type DbSet
        public DbSet<User> Users { get; set; }
        public DbSet<Project> Projects { get; set; }
    }
}
