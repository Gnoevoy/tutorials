﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CodeFirst
{
    public class User
    {
        public User()
        {
            this.Projects = new HashSet<Project>();
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Required]
        [ScaffoldColumn(false)]
        public Int32 UserID { get; set; }
        [MaxLength(50)]
        [Display(Name = "User Login")]
        [Required]
        public String Login { get; set; }
        [MaxLength(50)]
        [Display(Name = "User Name")]
        [Required]
        public String Name { get; set; }
        [MaxLength(50)]
        [Display(Name = "User Surname")]
        [Required]
        public String Surname { get; set; }

        public virtual ICollection<Project> Projects { get; set; }
    }

    public class Project
    {
        public Project()
        {
            this.Users = new HashSet<User>();
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Required]
        public Int32 ProjectID { get; set; }
        [MaxLength(50)]
        public String ProjectName { get; set; }
        [MaxLength(50)]
        public String Description { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
