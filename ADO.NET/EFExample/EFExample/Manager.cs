﻿using CodeFirst;
using System;
using System.Linq;

namespace EFExample
{
    public class Manager
    {
        public Manager()
        {
            Console.WriteLine("List before insert:");
            //SelectUsers();
            //SelectProjects();

            //AddUser(8, "User8", "Alfred", "Johns");
            //UpdateUser(8);
            //DeleteUser(8);

            Console.WriteLine("\nList after insert:\n");
            //SelectUsers();
            AddProject(new Project { ProjectID = 5, ProjectName = "Project5", Description = "Desc of Project5" });

            Console.ReadKey();
        }

        public void AddUser(Int32 idParam, String loginParam, String nameParam, String surnameParam)
        {
            var user = new User
            {
                UserID = idParam,
                Login = loginParam,
                Name = nameParam,
                Surname = surnameParam
            };

            // Create context object
            var context = new SampleContext();

            // Insert data in table User with LINQ
            context.Users.Add(user);

            // Save changes in DB
            context.SaveChanges();
        }

        public void AddProject(Project projParam)
        {
            var context = new SampleContext();
            var user = context.Users.Find(2);

            user.Projects.Add(projParam);
            projParam.Users.Add(user);

            context.SaveChanges();
        }

        public void UpdateProject(Int32 idParam)
        {
            var context = new SampleContext();

            var project = context.Projects.Where(c => c.ProjectID == idParam).FirstOrDefault();
            project.ProjectName = "Project10";

            context.SaveChanges();
        }

        public void SelectUsers()
        {
            var context = new SampleContext();
            var users = context.Users.ToList();

            foreach (var item in users)
            {
                Console.WriteLine(item.Login + "\t" + item.Name);
            }
        }

        //public void SelectProjects()
        //{
        //    var context = new SampleContext();
        //    var users = context.Users.ToList();
        //    var ups = context.UsersProjects.ToList();
        //    var projects = context.Projects.ToList();

        //    var data = from user in users
        //               join up in ups on user.UserID equals up.UserID
        //               join project in projects on up.ProjectID equals (Int32)project.ProjectID
        //               select new
        //               {
        //                   Login = user.Login,
        //                   Name = user.Name,
        //                   ProjectName = project.ProjectName,
        //                   Description = project.Description,
        //               };

        //    Console.WriteLine("User login\tName\t    Project\tDescription\n");

        //    foreach (var item in data)
        //    {
        //        Console.WriteLine("{0, -16}{1, -10}{2, -10}{3, -20}", item.Login, item.Name, item.ProjectName, item.Description);
        //    }
        //}

        public void UpdateUser(Int32 idParam)
        {
            var context = new SampleContext();
            var cust = context.Users.Where(c => c.UserID == idParam).FirstOrDefault();
            cust.Login = "User10";

            context.SaveChanges();
        }

        public void DeleteUser(Int32 idParam)
        {
            var context = new SampleContext();
            var user = context.Users.Where(c => c.UserID == idParam).FirstOrDefault();
            context.Users.Remove(user);

            context.SaveChanges();
        }

        public void DeleteProject(Int32 idProj, User user)
        {
            var context = new SampleContext();

            var project = context.Projects.Find(idProj);
            user = context.Users.Find(2);

            context.Projects.Remove(project);
            user.Projects.Remove(project);

            context.SaveChanges();
        }
    }
}
