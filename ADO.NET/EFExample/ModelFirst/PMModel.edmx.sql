
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 01/23/2017 04:55:41
-- Generated from EDMX file: F:\Kostya's files\Programming\Tutorials\ADO.NET\EFExample\ModelFirst\PMModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [testDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[ProjectSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ProjectSet];
GO
IF OBJECT_ID(N'[dbo].[UserSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'ProjectSet'
CREATE TABLE [dbo].[ProjectSet] (
    [ProjectID] int IDENTITY(1,1) NOT NULL,
    [ProjectName] nvarchar(50)  NOT NULL,
    [Description] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'UserSet'
CREATE TABLE [dbo].[UserSet] (
    [UserID] int IDENTITY(1,1) NOT NULL,
    [Login] nvarchar(50)  NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Surname] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'UserProject'
CREATE TABLE [dbo].[UserProject] (
    [User_UserID] int  NOT NULL,
    [Project_ProjectID] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ProjectID] in table 'ProjectSet'
ALTER TABLE [dbo].[ProjectSet]
ADD CONSTRAINT [PK_ProjectSet]
    PRIMARY KEY CLUSTERED ([ProjectID] ASC);
GO

-- Creating primary key on [UserID] in table 'UserSet'
ALTER TABLE [dbo].[UserSet]
ADD CONSTRAINT [PK_UserSet]
    PRIMARY KEY CLUSTERED ([UserID] ASC);
GO

-- Creating primary key on [User_UserID], [Project_ProjectID] in table 'UserProject'
ALTER TABLE [dbo].[UserProject]
ADD CONSTRAINT [PK_UserProject]
    PRIMARY KEY CLUSTERED ([User_UserID], [Project_ProjectID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [User_UserID] in table 'UserProject'
ALTER TABLE [dbo].[UserProject]
ADD CONSTRAINT [FK_UserProject_User]
    FOREIGN KEY ([User_UserID])
    REFERENCES [dbo].[UserSet]
        ([UserID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Project_ProjectID] in table 'UserProject'
ALTER TABLE [dbo].[UserProject]
ADD CONSTRAINT [FK_UserProject_Project]
    FOREIGN KEY ([Project_ProjectID])
    REFERENCES [dbo].[ProjectSet]
        ([ProjectID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserProject_Project'
CREATE INDEX [IX_FK_UserProject_Project]
ON [dbo].[UserProject]
    ([Project_ProjectID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------